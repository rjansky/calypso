#ifndef XAODFASERTRACKING_STRIPCLUSTERAUXCONTAINER_H
#define XAODFASERTRACKING_STRIPCLUSTERAUXCONTAINER_H

// System include(s):
#include <vector>

// Core include(s):
#include "xAODCore/AuxContainerBase.h"
#include "AthLinks/ElementLink.h"

namespace xAOD {
    /// Definition of the current Strip Cluster auxiliary container
    ///
    /// All reconstruction code should attach the typedefed auxiliary
    /// container to the xAOD::StripClusterContainer, so it will be easy to change
    /// the container type as we get new I/O technologies for these
    /// objects.
    ///
    class StripClusterAuxContainer : public AuxContainerBase {
    
    public:
    /// Default constructor
    StripClusterAuxContainer();
    /// Dumps contents (for debugging)
    void dump() const;
    
    private:
    std::vector< uint64_t >      id;
    std::vector< std::vector< uint64_t > > rdoIdentifierList;
    
    std::vector< float    >      localX;
    std::vector< float    >      localY;
    std::vector< float    >      localXError;
    std::vector< float    >      localYError;
    std::vector< float    >      localXYCorrelation;
    
    std::vector< float    >      globalX;
    std::vector< float    >      globalY;
    std::vector< float    >      globalZ;
    
    }; // class StripClusterAuxContainer
}

// Set up a CLID for the container:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::StripClusterAuxContainer , 1314387539 , 1 )
 
#endif // XAODFASERTRACKING_STRIPCLUSTERAUXCONTAINER_H