This repository contains plots and pictures of the FASER magnetic field.

The file 'FASERBField_xyz_Map.root' was created from 'FASERBField_xyz.root' using the root script 'printBField' :

printBField("../FASERBField_xyz.root","bField","FASERBField_xyz_Map.root",-0.1,0.1,0.,4.4,41,880)

It contains histograms of the FASER magnetic field map in xy,zx and zy.

The files 'xy.png', 'xz.png' and 'yz.png’ are screen shots of the above histograms.