/*
 * translateBFieldMap.cpp
 *
 *  Created on: 29 Jan 2018
 *      Author: jhrdinka
 */

/// This root file translates a txt/csv bField map into a root ttree

/// @param inFile The path to the txt/csv input file
/// @param outFile The name of the output root file
/// @param outTreeName The name of the tree of the output file
/// @param rz Set this flag to true if the magnetic field map is given in rz
/// cooridantes instead of cartesian coordinates
/// @param bScalor A scalor for the magnetic field to be applied
/// @param lScalor A scalor to the length to be applied
void
translateBFieldMap(std::string inFile,
                   std::string outFile,
                   std::string outTreeName,
                   bool        rz,
                   double      bScalor = 1.,
                   double      lScalor = 1.)
{

  std::cout << "Registering new ROOT output File : " << outFile << std::endl;

  TFile* outputFile = TFile::Open(outFile.c_str(), "RECREATE");
  if (!outputFile) std::cout << "Could not open '" << outFile << std::endl;
  TTree* outputTree = new TTree(outTreeName.c_str(), outTreeName.c_str());
  if (!outputTree) std::cout << "Could not allocate tree" << std::endl;

  if (rz) {

    /// [1] Read in field map file
    std::cout << "Opening new txt/csv input File : " << outFile << std::endl;
    std::ifstream map_file(inFile.c_str(), std::ios::in);
    // [1] Read in file and fill values
    std::string line;
    double      rpos = 0., zpos = 0.;
    double      br = 0., bz = 0.;

    // The position value in r
    double r;
    outputTree->Branch("r", &r);

    double z;
    outputTree->Branch("z", &z);
    // The BField value in r
    double Br;
    outputTree->Branch("Br", &Br);

    double Bz;
    outputTree->Branch("Bz", &Bz);

    while (std::getline(map_file, line)) {
      if (line.empty() || line[0] == '%' || line[0] == '#'
          || line.find_first_not_of(' ') == std::string::npos)
        continue;

      std::istringstream tmp(line);
      tmp >> rpos >> zpos >> br >> bz;

      z  = zpos * lScalor;
      r  = rpos * lScalor;
      Br = br * bScalor;
      Bz = bz * bScalor;
      outputTree->Fill();
    }

    outputFile->cd();

    outputFile->cd();
    outputTree->Write();

    map_file.close();

  } else {
    /// [1] Read in field map file
    std::cout << "Opening new txt/csv input File : " << outFile << std::endl;
    std::ifstream map_file(inFile.c_str(), std::ios::in);

    // The position values in xy
    double x;
    outputTree->Branch("x", &x);

    double y;
    outputTree->Branch("y", &y);

    double z;
    outputTree->Branch("z", &z);

    // The BField values in xy
    double Bx;
    outputTree->Branch("Bx", &Bx);

    double By;
    outputTree->Branch("By", &By);

    double Bz;
    outputTree->Branch("Bz", &Bz);

    std::string line;
    double      xpos = 0., ypos = 0., zpos = 0.;
    double      bx = 0., by = 0., bz = 0.;
    while (std::getline(map_file, line)) {
      if (line.empty() || line[0] == '%' || line[0] == '#'
          || line.find_first_not_of(' ') == std::string::npos)
        continue;

      std::istringstream tmp(line);
      tmp >> xpos >> ypos >> zpos >> bx >> by >> bz;

      x  = xpos * lScalor;
      y  = ypos * lScalor;
      z  = zpos * lScalor;
      Bx = bx * bScalor;
      By = by * bScalor;
      Bz = bz * bScalor;

      outputTree->Fill();
    }

    outputFile->cd();
    outputTree->Write();

    map_file.close();
  }
}
