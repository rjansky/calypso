#
#  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.AthConfigFlags import AthConfigFlags

def FaserGeometryCfg (flags):
    acc = ComponentAccumulator()

    from FaserGeoModel.ScintGMConfig import ScintGeometryCfg
    acc.merge(ScintGeometryCfg(flags))
    # if (flags.Detector.Simulate and flags.Beam.Type == "cosmics") or flags.Detector.SimulateCavern:
    #     from CavernInfraGeoModel.CavernInfraGeoModelConf import CavernInfraDetectorTool
    #     gms.DetectorTools += [ CavernInfraDetectorTool() ]
    return acc
