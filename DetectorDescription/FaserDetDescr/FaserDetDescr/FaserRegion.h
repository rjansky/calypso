/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// FaserRegion.h, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

#ifndef FASERDETDESCR_FASERREGION_H
#define FASERDETDESCR_FASERREGION_H 1

#include <assert.h>

// use these macros to check a given FaserRegion for its validity
#define validFaserRegion(region) ( (region<FaserDetDescr::fNumFaserRegions) && (region>=FaserDetDescr::fFirstFaserRegion) )
#define assertFaserRegion(region) ( assert(validFaserRegion(region)) )

namespace FaserDetDescr {

 /** @enum FaserRegion
   
     A simple enum of FASER regions and sub-detectors.

   */

   enum FaserRegion {       
        // Unset
            fUndefinedFaserRegion = 0,
        // first Geometry element in enum, used in e.g. loops
            fFirstFaserRegion     = 1,
        // FASER Detector setup: geometrical ones
            fFaserScint           = 1,
            fFaserTracker         = 2,
            fFaserCalo            = 3,
            fFaserCavern          = 4,
        // number of defined GeoIDs
            fNumFaserRegions      = 5
   };

} // end of namespace

#endif // FASERDETDESCR_FASERREGION
