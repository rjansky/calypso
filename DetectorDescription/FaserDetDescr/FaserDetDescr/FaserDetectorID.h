/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#ifndef FASERDETDESCR_FASERDETECTORID_H
#define FASERDETDESCR_FASERDETECTORID_H
/**
 * @file FaserDetectorID.h
 *
 * @brief This is an Identifier helper class for the all
 *  subdetectors. This class is a factory for creating compact
 *  Identifier objects and IdentifierHash or hash ids. And it also
 *  allows decoding of these ids.
 *
 */

//<<<<<< INCLUDES                                                       >>>>>>

#include "FaserDetDescr/FaserDetectorIDBase.h"
#include "Identifier/Identifier.h"
#include "Identifier/IdentifierHash.h"
#include "Identifier/Range.h"
#include "Identifier/IdHelper.h"
#include "IdDict/IdDictFieldImplementation.h"
#include "AthenaKernel/CLASS_DEF.h"

#include <string>
#include <assert.h>
#include <algorithm>

//<<<<<< PUBLIC DEFINES                                                 >>>>>>
//<<<<<< PUBLIC CONSTANTS                                               >>>>>>
//<<<<<< PUBLIC TYPES                                                   >>>>>>

class IdDictDictionary;

//<<<<<< PUBLIC VARIABLES                                               >>>>>>
//<<<<<< PUBLIC FUNCTIONS                                               >>>>>>
//<<<<<< CLASS DECLARATIONS                                             >>>>>>

/**
 **  @class FaserDetectorID
 **  
 **  @brief This is an Identifier helper class for the FASER
 **  subdetectors. This class is a factory for creating compact
 **  Identifier objects and IdentifierHash or hash ids. And it also
 **  allows decoding of these ids.
 **
 **  Definition and the range of values for the levels of the
 **  identifier are:
 **
 ** @verbatim
 **    element           range              meaning
 **    -------           -----              -------
 **
 **    station
 **          veto        0 - 1         front/back
 **          trigger       0           single station
 **          preshower     0           single station
 **          sct         0 - 2         front, middle, back
 **          ecal          0           single station
 **    plane               
 **          veto          0           single plane
 **          trigger       0           single plane
 **          preshower     0           single plane
 **          sct         0 - 2         three planes per station
 **          ecal          0           single plane
 **    module
 **          veto          0           single plate
 **          trigger     0 - 1         upper/lower plate
 **          preshower     0           single plate
 **          sct         0 - 7         eight modules per plane
 **          ecal        0 - 3         four modules
 **    submodule
 **          veto          0           no segmentation
 **          trigger       0           no segmentation
 **          preshower     0           no segmentation
 **          sct         0 - 1         two sides per module
 **          ecal        0 - 66        scintillator layers
 **    sensor
 **          veto          0           single pmt
 **          trigger     0 - 1         left/right pmt
 **          preshower     0           single pmt
 **          sct         0 - 767       strips
 **          ecal          0           one tower per layer
 ** @endverbatim
 **
 */
class FaserDetectorID : public FaserDetectorIDBase
{
public:
        
    /// @name public typedefs
    //@{
    typedef Identifier::size_type                       size_type; 
    typedef std::vector<Identifier>::const_iterator     const_id_iterator;
    typedef MultiRange::const_identifier_factory        const_expanded_id_iterator;
    //@}

    /// @name strutors
    //@{
    FaserDetectorID(void);
    virtual ~FaserDetectorID(void) = default;
    //@}
        
    /// @name Creators for station,  ids and pmt ids
    //@{
    /// For a single module
    virtual Identifier  module_id( int station,
                           int plane, 
                           int module ) const = 0;

    /// For a module from a submodule id
    Identifier  module_id ( const Identifier& submodule_id ) const;

    /// For a single submodule
    virtual Identifier  submodule_id ( int station,
                               int plane,
                               int module,
                               int submodule ) const = 0; 

    /// For a single submodule from a sensor id
    Identifier  submodule_id ( const Identifier& sensor_id ) const;

    /// From hash - optimized
    Identifier submodule_id( IdentifierHash submodule_hash) const;

    /// For an individual sensor
    virtual Identifier  sensor_id   ( int station,
                           int plane,
                           int module, 
                           int submodule, 
                           int sensor ) const = 0; 


    Identifier  sensor_id ( const Identifier& submodule_id, 
                            int sensor ) const;

    //@}

    /// @name Hash table maximum sizes
    //@{
    size_type   submodule_hash_max          (void) const;
    size_type   sensor_hash_max             (void) const;
    //@}

    /// @name Access to all ids
    //@{
    /// Iterators over full set of ids. Submodule iterator is sorted
    const_id_iterator   submodule_begin                 (void) const;
    const_id_iterator   submodule_end                   (void) const;
    /// For sensor ids, only expanded id iterators are available. Use
    /// following "sensor_id" method to obtain a compact identifier
    const_expanded_id_iterator  sensor_begin             (void) const;  
    const_expanded_id_iterator  sensor_end               (void) const;
    //@}


    /// submodule hash from id - optimized
    IdentifierHash      submodule_hash      (Identifier submodule_id) const;


    /// Values of different levels (failure returns 0)
    int         station         (const Identifier& id) const;  
    int         plane           (const Identifier& id) const;
    int         module          (const Identifier& id) const;
    int         submodule       (const Identifier& id) const; 
    int         sensor          (const Identifier& id) const;

    // /// Max values for each field (-999 == failure)
    int         station_max     (const Identifier& id) const;
    int         plane_max       (const Identifier& id) const;
    int         module_max      (const Identifier& id) const;
    int         submodule_max   (const Identifier& id) const;
    int         sensor_max      (const Identifier& id) const;
    //@}

    // /// @name module eta/phi navigation
    // //@{
    // /// Previous wafer hash in phi (return == 0 for neighbor found)
    // int         get_prev_in_phi (const IdentifierHash& id, IdentifierHash& prev) const;
    // /// Next wafer hash in phi (return == 0 for neighbor found)
    // int         get_next_in_phi (const IdentifierHash& id, IdentifierHash& next) const;
    // /// Previous wafer hash in eta (return == 0 for neighbor found)
    // int         get_prev_in_eta (const IdentifierHash& id, IdentifierHash& prev) const;
    // /// Next wafer hash in eta (return == 0 for neighbor found)
    // int         get_next_in_eta (const IdentifierHash& id, IdentifierHash& next) const;
    // /// Wafer hash on other side
    // int         get_other_side  (const IdentifierHash& id, IdentifierHash& other) const;
    
    // // To check for when phi wrap around may be needed, use
    // bool        is_phi_module_max(const Identifier& id) const;
    // /// For the barrel
    // bool        is_eta_module_min(const Identifier& id) const;
    // /// For the barrel
    // bool        is_eta_module_max(const Identifier& id) const;
    // //@}

    // /// @name contexts to distinguish wafer id from pixel id
    // //@{
    // IdContext   wafer_context           (void) const;
    // IdContext   strip_context           (void) const;
    // //@}

    /// @name methods from abstract interface - slower than opt version
    //@{
    /// Create compact id from hash id (return == 0 for OK)
    virtual int         get_id          (const IdentifierHash& hash_id,
                                         Identifier& id,
                                         const IdContext* context = 0) const;
    
    /// Create hash id from compact id (return == 0 for OK)
    virtual int         get_hash        (const Identifier& id, 
                                         IdentifierHash& hash_id,
                                         const IdContext* context = 0) const;
    //@}

    /// Return the lowest bit position used in the channel id
    int                 base_bit        (void) const;

    // /// Calculate a channel offset between the two identifiers.
    // Identifier::diff_type calc_offset(const Identifier& base,
    //                                   const Identifier& target) const;

    // /// Create an identifier with a given base and channel offset
    // Identifier strip_id_offset(const Identifier& base,
    //                            Identifier::diff_type offset) const;

    /// @name interaction with id dictionary
    //@{
    /// Create sensor Identifier from expanded id, which is returned by the
    /// id_iterators
    Identifier          sensor_id        (const ExpandedIdentifier& sensor_id) const;

    /// Create expanded id from compact id (return == 0 for OK)
    void                get_expanded_id (const Identifier& id,
                                         ExpandedIdentifier& exp_id,
                                         const IdContext* context = 0) const;

    /// Initialization from the identifier dictionary
    virtual int         initialize_from_dictionary(const IdDictMgr& dict_mgr);

private:

    typedef std::vector<Identifier>     id_vec;
    typedef id_vec::const_iterator      id_vec_it;
    typedef std::vector<unsigned short> hash_vec;
    typedef hash_vec::const_iterator    hash_vec_it;

    void submodule_id_checks ( int station, 
                               int plane, 
                               int module, 
                               int submodule ) const;

    void sensor_id_checks ( int station, 
                            int plane, 
                            int module, 
                            int submodule,   
                            int sensor) const;

    int         initLevelsFromDict(void);

    int         init_hashes(void);

    int         init_neighbors(void);

    size_type                   m_faser_region_index;
    size_type                   m_SUBDET_INDEX;
    size_type                   m_PART_INDEX;
    size_type                   m_STATION_INDEX;
    size_type                   m_PLANE_INDEX;
    size_type                   m_MODULE_INDEX;
    size_type                   m_SUBMODULE_INDEX;
    size_type                   m_SENSOR_INDEX;
        
    const IdDictDictionary*     m_dict;
    MultiRange                  m_full_submodule_range;
    MultiRange                  m_full_sensor_range;
    size_type                   m_submodule_hash_max;
    size_type                   m_sensor_hash_max;
    id_vec                      m_submodule_vec;
    // hash_vec                    m_prev_phi_wafer_vec;
    // hash_vec                    m_next_phi_wafer_vec;
    // hash_vec                    m_prev_eta_wafer_vec;
    // hash_vec                    m_next_eta_wafer_vec;   

    IdDictFieldImplementation   m_subdet_impl	;
    IdDictFieldImplementation   m_part_impl	;
    IdDictFieldImplementation   m_station_impl	;
    IdDictFieldImplementation   m_plane_impl	;
    IdDictFieldImplementation   m_module_impl   ;
    IdDictFieldImplementation   m_submodule_impl;
    IdDictFieldImplementation   m_sensor_impl	;
};
    

//<<<<<< INLINE PUBLIC FUNCTIONS                                        >>>>>>

/////////////////////////////////////////////////////////////////////////////
//<<<<<< INLINE MEMBER FUNCTIONS                                        >>>>>>
/////////////////////////////////////////////////////////////////////////////

//using the macros below we can assign an identifier (and a version)
//This is required and checked at compile time when you try to record/retrieve
CLASS_DEF(FaserDetectorID, 125694213, 1)

//----------------------------------------------------------------------------
// inline Identifier  
// FaserDetectorID::module_id ( int station,
//                              int plane,
//                              int module ) const 
// {
    
//     // Build identifier
//     Identifier result((Identifier::value_type)0);

//     // Pack fields independently
//     m_scint_impl.pack    (scint_field_value(), result);
//     m_veto_impl.pack     (veto_field_value(),  result);
//     m_station_impl.pack  (station,             result);

//     return result;
// }

//----------------------------------------------------------------------------
inline Identifier  
FaserDetectorID::module_id ( const Identifier& submodule_id ) const
{
    Identifier result(submodule_id);
    //  Reset the plate and pmt fields
    m_submodule_impl.reset(result);
    m_sensor_impl.reset(result);
    return (result);
}

//----------------------------------------------------------------------------
// inline Identifier
// FaserDetectorID::submodule_id ( int station, 
//                                 int plane,
//                                 int module,
//                                 int submodule ) const 
// {
//     // Build identifier
//     Identifier result((Identifier::value_type)0);

//     // Pack fields independently
//     m_scint_impl.pack    (scint_field_value(), result);
//     m_veto_impl.pack     (veto_field_value(),  result);
//     m_station_impl.pack  (station,             result);
//     m_plane_impl.pack    (plane,               result);
//     m_module_impl.pack   (module,              result);
//     m_submodule_impl.pack(submodule,           result);

//     return result;
// }

//----------------------------------------------------------------------------
inline Identifier
FaserDetectorID::submodule_id ( const Identifier& sensor_id ) const
{
    Identifier result(sensor_id);
    // reset the strip field
    m_sensor_impl.reset(result);
    return (result);
}

inline IdentifierHash
FaserDetectorID::submodule_hash      (Identifier submodule_id) const 
{
    id_vec_it it = std::lower_bound(m_submodule_vec.begin(), 
                                    m_submodule_vec.end(), 
                                    submodule_id);
    // Require that submodule_id matches the one in vector
    if (it != m_submodule_vec.end() && submodule_id == (*it)) {
        return (it - m_submodule_vec.begin());
    }
    IdentifierHash result;
    return (result); // return hash in invalid state
}

//----------------------------------------------------------------------------
// inline Identifier
// FaserDetectorID::sensor_id ( int station,  
//                              int plane, 
//                              int module,
//                              int submodule,
//                              int sensor ) const
// {
//     // Build identifier
//     Identifier result((Identifier::value_type)0);

//     // Pack fields independently
//     m_scint_impl.pack    (scint_field_value(), result);
//     m_veto_impl.pack     (veto_field_value(),  result);
//     m_station_impl.pack  (station,             result);
//     m_plane_impl.pack    (plane,               result);
//     m_module_impl.pack   (module,              result);
//     m_submodule_impl.pack(submodule,           result);
//     m_sensor_impl.pack   (sensor,              result);

//     return result;
// }

//----------------------------------------------------------------------------
inline Identifier  
FaserDetectorID::sensor_id ( const Identifier& submodule_id, int sensor ) const
{
	// Build identifier
    Identifier result(submodule_id);
  
    // Reset sensor and then add in value
    m_sensor_impl.reset   (result);
 	m_sensor_impl.pack    (sensor, result);
  
	return result;  
}

//----------------------------------------------------------------------------
inline int 
FaserDetectorID::station       (const Identifier& id) const
{
    return (m_station_impl.unpack(id));
}

//----------------------------------------------------------------------------
inline int 
FaserDetectorID::plane         (const Identifier& id) const
{
    return (m_plane_impl.unpack(id));
}
//----------------------------------------------------------------------------
inline int 
FaserDetectorID::module        (const Identifier& id) const
{
    return (m_module_impl.unpack(id));
}

//----------------------------------------------------------------------------
inline int 
FaserDetectorID::submodule     (const Identifier& id) const
{
    return (m_submodule_impl.unpack(id));
}

//----------------------------------------------------------------------------
inline int 
FaserDetectorID::sensor       (const Identifier& id) const
{
    return (m_sensor_impl.unpack(id));
}


#endif // FASERDETDESCR_FASERDETECTORID_H
