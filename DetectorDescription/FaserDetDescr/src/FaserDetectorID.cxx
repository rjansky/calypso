/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/***************************************************************************
 Scintillator identifier package
 -------------------------------------------
***************************************************************************/

//<<<<<< INCLUDES                                                       >>>>>>
#include "GaudiKernel/MsgStream.h"

#include "FaserDetDescr/FaserDetectorID.h"
#include "Identifier/IdentifierHash.h"
#include "IdDict/IdDictDefs.h"  
#include <set>
#include <algorithm>
#include <iostream>

//<<<<<< PRIVATE DEFINES                                                >>>>>>
//<<<<<< PRIVATE CONSTANTS                                              >>>>>>
//<<<<<< PRIVATE TYPES                                                  >>>>>>
//<<<<<< PRIVATE VARIABLE DEFINITIONS                                   >>>>>>
//<<<<<< PUBLIC VARIABLE DEFINITIONS                                    >>>>>>
//<<<<<< CLASS STRUCTURE INITIALIZATION                                 >>>>>>
//<<<<<< PRIVATE FUNCTION DEFINITIONS                                   >>>>>>
//<<<<<< PUBLIC FUNCTION DEFINITIONS                                    >>>>>>
//<<<<<< MEMBER FUNCTION DEFINITIONS                                    >>>>>>


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


FaserDetectorID::FaserDetectorID(void)
        :
        m_faser_region_index(0),
        m_SUBDET_INDEX(0),
        m_PART_INDEX(1),
        m_STATION_INDEX(2),
        m_PLANE_INDEX(3),
        m_MODULE_INDEX(4),
        m_SUBMODULE_INDEX(5),
        m_SENSOR_INDEX(6),
        m_dict(0),
        m_submodule_hash_max(0),
        m_sensor_hash_max(0)
{
}

int
FaserDetectorID::init_hashes(void)
{

    //
    // create a vector(s) to retrieve the hashes for compact ids. For
    // the moment, we implement a hash for submodules but NOT for sensors
    //
    MsgStream log(m_msgSvc, "FaserDetectorID");
    // wafer hash
    m_submodule_hash_max = m_full_submodule_range.cardinality();
    m_submodule_vec.resize(m_submodule_hash_max);
    unsigned int nids = 0;
    std::set<Identifier> ids;
    for (unsigned int i = 0; i < m_full_submodule_range.size(); ++i) {
        const Range& range = m_full_submodule_range[i];
        Range::const_identifier_factory first = range.factory_begin();
        Range::const_identifier_factory last  = range.factory_end();
        for (; first != last; ++first) {
            const ExpandedIdentifier& exp_id = (*first);
            Identifier id = submodule_id(exp_id[m_STATION_INDEX],
                                         exp_id[m_PLANE_INDEX],
                                         exp_id[m_MODULE_INDEX],
                                         exp_id[m_SUBMODULE_INDEX]);
            if(!(ids.insert(id)).second) {
                log << MSG::ERROR << " FaserDetectorID::init_hashes "
                    << " Error: duplicated id for plate id. nid " << nids
                    << " compact id " << id.getString()
                    << " id " << (std::string)exp_id << endmsg;
                return (1);
            }
            nids++;
        }
    }
    if(ids.size() != m_submodule_hash_max) {
        log << MSG::ERROR << " FaserDetectorID::init_hashes "
            << " Error: set size NOT EQUAL to hash max. size " << ids.size()
            << " hash max " << m_submodule_hash_max 
            << endmsg;
        return (1);
    }

    nids = 0;
    std::set<Identifier>::const_iterator first = ids.begin();
    std::set<Identifier>::const_iterator last  = ids.end();
    for (; first != last && nids < m_submodule_vec.size(); ++first) {
        m_submodule_vec[nids] = (*first);
        nids++;
    }

    // sensor hash - we do not keep a vec for the sensors
    m_sensor_hash_max = m_full_sensor_range.cardinality();

    return (0);
}


int
FaserDetectorID::initialize_from_dictionary(const IdDictMgr& dict_mgr)
{
    MsgStream log(m_msgSvc, "FaserDetectorID");
    log << MSG::INFO << "Initialize from dictionary" << endmsg;
  
    // Check whether this helper should be reinitialized
    if (!reinitialize(dict_mgr)) {
        log << MSG::INFO << "Request to reinitialize not satisfied - tags have not changed" << endmsg;
        return (0);
    }
    else {
        if (m_msgSvc) {
            log << MSG::DEBUG << "(Re)initialize" << endmsg;
        }
        else {
            std::cout  << " DEBUG (Re)initialize" << std::endl;
        }
    }

    // init base object
    if(FaserDetectorIDBase::initialize_from_dictionary(dict_mgr)) return (1);

    // Register version of InnerDetector dictionary 
    if (register_dict_tag(dict_mgr, "Scintillator")) return(1);

    m_dict = dict_mgr.find_dictionary ("Scintillator"); 
    if(!m_dict) {
        log << MSG::ERROR << " FaserDetectorID::initialize_from_dict - cannot access Scintillator dictionary " << endmsg;
        return 1;
    }

    // Initialize the field indices
    if(initLevelsFromDict()) return (1);

    //
    // Build multirange for the valid set of identifiers
    //


    // Find value for the field Scintillator
    const IdDictDictionary* faserDict = dict_mgr.find_dictionary ("FASER"); 
    int scintField   = -1;
    if (faserDict->get_label_value("subdet", "Scintillator", scintField)) {
        log << MSG::ERROR << "Could not get value for label 'Scintillator' of field 'subdet' in dictionary " 
            << faserDict->m_name
            << endmsg;
        return (1);
    }

    // Find value for the field Veto
    int vetoField   = -1;
    if (m_dict->get_label_value("part", "Veto", vetoField)) {
        log << MSG::ERROR << "Could not get value for label 'Veto' of field 'part' in dictionary " 
            << m_dict->m_name
            << endmsg;
        return (1);
    }
    if (m_msgSvc) {
        log << MSG::DEBUG << " VetoID::initialize_from_dict " 
            << "Found field values: Veto "  
            << vetoField
            << std::endl;
    }
    else {
        std::cout << " DEBUG VetoID::initialize_from_dict " 
                  << "Found field values: Veto "  
                  << vetoField
                  << std::endl;
    }
    
    // Set up id for region and range prefix
    ExpandedIdentifier region_id;
    region_id.add(scintField);
    region_id.add(vetoField);
    Range prefix;
    m_full_plate_range = m_dict->build_multirange(region_id, prefix, "plate");
    m_full_pmt_range = m_dict->build_multirange(region_id, prefix);

    
    if (m_msgSvc) {
        log << MSG::INFO << " VetoID::initialize_from_dict "  << endmsg;
        log << MSG::DEBUG  
            << "Plate range -> " << (std::string)m_full_plate_range
            <<   endmsg;
        log << MSG::DEBUG
            << "Pmt range -> " << (std::string)m_full_pmt_range
            << endmsg;
    }
    else {
        std::cout << " INFO VetoID::initialize_from_dict "  << std::endl;
        std::cout << " DEBUG  Plate range -> " << (std::string)m_full_plate_range
                  <<   std::endl;
        std::cout << " DEBUG Pmt range -> " << (std::string)m_full_pmt_range
                  << std::endl;
    }
    
    return 0;
}

int     
FaserDetectorID::initLevelsFromDict()
{


    MsgStream log(m_msgSvc, "FaserDetectorID");
    if(!m_dict) {
        log << MSG::ERROR << " FaserDetectorID::initLevelsFromDict - dictionary NOT initialized " << endmsg;
        return (1);
    }
    
    // Find out which identifier field corresponds to each level. Use
    // names to find each field/leve.

    m_SUBDET_INDEX              = 999;
    m_PART_INDEX                = 999;
    m_STATION_INDEX             = 999;
    m_PLANE_INDEX               = 999;
    m_MODULE_INDEX              = 999;
    m_SUBMODULE_INDEX           = 999;
    m_SENSOR_INDEX              = 999;
    

    // Save index to a Veto region for unpacking
    ExpandedIdentifier id; 
    id << scint_field_value() << veto_field_value();
    if (m_dict->find_region(id, m_veto_region_index)) {
        log << MSG::ERROR << "FaserDetectorID::initLevelsFromDict - unable to find veto region index: id, reg "  
            << (std::string)id << " " << m_veto_region_index
            << endmsg;
        return (1);
    }

    // Find a Vetp region
    IdDictField* field = m_dict->find_field("subdet");
    if (field) {
        m_SCINT_INDEX = field->m_index;
    }
    else {
        log << MSG::ERROR << "VetoID::initLevelsFromDict - unable to find 'subdet' field "  << endmsg;
        return (1);
    }
    field = m_dict->find_field("part");
    if (field) {
        m_VETO_INDEX = field->m_index;
    }
    else {
        log << MSG::ERROR << "VetoID::initLevelsFromDict - unable to find 'part' field "  << endmsg;
        return (1);
    }
    field = m_dict->find_field("station");
    if (field) {
        m_STATION_INDEX = field->m_index;
    }
    else {
        log << MSG::ERROR << "VetoID::initLevelsFromDict - unable to find 'station' field "  << endmsg;
        return (1);
    }
    field = m_dict->find_field("plate");
    if (field) {
        m_PLATE_INDEX = field->m_index;
    }
    else {
        log << MSG::ERROR << "VetoID::initLevelsFromDict - unable to find 'plate' field "   << endmsg;
        return (1);
    }
    field = m_dict->find_field("pmt");
    if (field) {
        m_PMT_INDEX = field->m_index;
    }
    else {
        log << MSG::ERROR<< "VetoID::initLevelsFromDict - unable to find 'pmt' field "  << endmsg;
        return (1);
    }
    
    // Set the field implementations: for station, plate, pmt

    const IdDictRegion& region = *m_dict->m_regions[m_veto_region_index];

    m_scint_impl      = region.m_implementation[m_SCINT_INDEX]; 
    m_veto_impl       = region.m_implementation[m_VETO_INDEX]; 
    m_station_impl    = region.m_implementation[m_STATION_INDEX]; 
    m_plate_impl      = region.m_implementation[m_PLATE_INDEX]; 
    m_pmt_impl        = region.m_implementation[m_PMT_INDEX]; 

    if (m_msgSvc) {
        log << MSG::DEBUG << "decode index and bit fields for each level: " << endmsg;
        log << MSG::DEBUG << "scint    "  << m_scint_impl.show_to_string() << endmsg;
        log << MSG::DEBUG << "veto     "  << m_veto_impl.show_to_string() << endmsg; 
        log << MSG::DEBUG << "station  "  << m_station_impl.show_to_string() << endmsg; 
        log << MSG::DEBUG << "plate    "  << m_plate_impl.show_to_string() << endmsg; 
        log << MSG::DEBUG << "pmt      "  << m_pmt_impl.show_to_string() << endmsg; 
    }
    else {
        std::cout << " DEBUG decode index and bit fields for each level: " << std::endl;
        std::cout << " DEBUG scint    "  << m_scint_impl.show_to_string() << std::endl;
        std::cout << " DEBUG veto     "  << m_veto_impl.show_to_string() << std::endl; 
        std::cout << " DEBUG station  "  << m_station_impl.show_to_string() << std::endl; 
        std::cout << " DEBUG plate    "  << m_plate_impl.show_to_string() << std::endl; 
        std::cout << " DEBUG pmt      "  << m_pmt_impl.show_to_string() << std::endl; 
    }
    
//      std::cout << "SCT_ID::initLevelsFromDict - found levels "       << std::endl;
//      std::cout << "subdet        "   << m_INDET_INDEX        << std::endl;
//      std::cout << "part          "   << m_SCT_INDEX          << std::endl;
//      std::cout << "barrel_endcap "   << m_BARREL_EC_INDEX    << std::endl;
//      std::cout << "layer or disk "   << m_LAYER_DISK_INDEX   << std::endl;
//      std::cout << "phi_module    "   << m_PHI_MODULE_INDEX   << std::endl;
//      std::cout << "eta_module    "   << m_ETA_MODULE_INDEX   << std::endl;
//      std::cout << "side          "   << m_SIDE_INDEX         << std::endl;
//      std::cout << "strip         "   << m_STRIP_INDEX        << std::endl;

    std::cout << "scint "  << m_scint_impl.decode_index() << " " 
              <<   (std::string)m_scint_impl.ored_field() << " " 
              << std::hex    << m_scint_impl.mask() << " " 
              << m_scint_impl.zeroing_mask() << " " 
              << std::dec    << m_scint_impl.shift() << " "
              << m_scint_impl.bits() << " "
              << m_scint_impl.bits_offset()
              << std::endl;
    std::cout << "veto"     << m_veto_impl.decode_index() << " " 
              <<   (std::string)m_veto_impl.ored_field() << " " 
              << std::hex    << m_veto_impl.mask() << " " 
              << m_veto_impl.zeroing_mask() << " " 
              << std::dec    << m_veto_impl.shift() << " "
              << m_veto_impl.bits() << " "
              << m_veto_impl.bits_offset()
              << std::endl;
    std::cout << "station"     << m_station_impl.decode_index() << " " 
              <<   (std::string)m_station_impl.ored_field() << " " 
              << std::hex    << m_station_impl.mask() << " " 
              << m_station_impl.zeroing_mask() << " " 
              << std::dec    << m_station_impl.shift() << " "
              << m_station_impl.bits() << " "
              << m_station_impl.bits_offset()
              << std::endl;
    std::cout << "plate"<< m_plate_impl.decode_index() << " " 
              <<   (std::string)m_plate_impl.ored_field() << " " 
              << std::hex    << m_plate_impl.mask() << " " 
              << m_plate_impl.zeroing_mask() << " " 
              << std::dec    << m_plate_impl.shift() << " "
              << m_plate_impl.bits() << " "
              << m_plate_impl.bits_offset()
              << std::endl;
    std::cout << "pmt" << m_pmt_impl.decode_index() << " " 
              <<   (std::string)m_pmt_impl.ored_field() << " " 
              << std::hex    << m_pmt_impl.mask() << " " 
              << m_pmt_impl.zeroing_mask() << " " 
              << std::dec    << m_pmt_impl.shift() << " "
              << m_pmt_impl.bits() << " "
              << m_pmt_impl.bits_offset()
              << std::endl;
    return (0);
}

