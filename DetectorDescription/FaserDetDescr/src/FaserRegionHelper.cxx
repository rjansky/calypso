/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// FaserRegionName.cxx, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

#include "FaserDetDescr/FaserRegionHelper.h"

namespace FaserDetDescr {

  const char * FaserRegionHelper::getName( int region ) {
    return getName( FaserDetDescr::FaserRegion( region ) );
  }

  const char * FaserRegionHelper::getName( const FaserDetDescr::FaserRegion region ) {

    if      ( region == FaserDetDescr::fFaserScint   ) return "FaserScint";
    else if ( region == FaserDetDescr::fFaserTracker ) return "FaserTracker";
    else if ( region == FaserDetDescr::fFaserCalo    ) return "FaserCalo";
    else if ( region == FaserDetDescr::fFaserCavern  ) return "FaserCavern";
    else                                               return "UndefinedFaserRegion";

  }

} // end of namespace

