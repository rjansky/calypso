#ifndef LEGACYBASE_DICT_H
#define LEGACYBASE_DICT_H

//
// Declaration of classes that need dictionaries
//

#include <vector>
#include <map>

#include "LegacyBase/FaserEvent.h"
#include "LegacyBase/FaserDigi.h"
#include "LegacyBase/FaserSensorHit.h"
#include "LegacyBase/FaserCaloHit.h"
#include "LegacyBase/FaserCaloDigi.h"
#include "LegacyBase/FaserTruthParticle.h"
#include "LegacyBase/FaserCluster.h"
#include "G4String.hh"
#include "G4AttDef.hh"
#include "G4AttValue.hh"
//#include "LegacyBase/FaserSpacePoint.h"

std::vector<FaserSensorHit*> aGarbage;
std::vector<FaserCaloHit*> aaGarbage;
std::vector<FaserDigi*> bGarbage;
std::vector<FaserCaloDigi*> bbGarbage;
std::vector<FaserTruthParticle*> cGarbage;
std::vector<FaserCluster*> hGarbage;
//std::vector<FaserSpacePoint*> iGarbage;

std::pair<G4String, G4AttDef> ddGarbage;
std::map<G4String, G4AttDef> dGarbage;
std::vector<G4AttValue> eGarbage;
std::map<G4int, G4double> fGarbage;
std::map<G4String, G4String> jjGarbage;
std::map<G4String, std::map<G4String, G4String>> jGarbage;

#endif
