################################################################################
# Package: LegacyConverter
################################################################################


# Declare the package name:
atlas_subdir( LegacyConverter )

# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          Control/AthenaBaseComps
			  xAOD/xAODFaserTruth
                          xAOD/xAODFaserTracking
			  Legacy/LegacyBase
                          GaudiKernel
                          PRIVATE
                          Control/AthenaKernel
                          )
                          

# External dependencies:
find_package( ROOT COMPONENTS Core Tree MathCore Hist RIO pthread EG )

# Component(s) in the package:
atlas_add_component( LegacyConverter
                     src/*.cxx
                     src/components/*.cxx
                     INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                     LINK_LIBRARIES ${ROOT_LIBRARIES} 
                     AthenaBaseComps xAODFaserTruth xAODFaserTracking LegacyBase GaudiKernel AthenaKernel 
)
# Install files from the package:
atlas_install_headers( LegacyConverter )
