# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

from AthenaCommon import CfgMgr

def getVetoDetectorTool(name="VetoDetectorTool", **kwargs):
    kwargs.setdefault("DetectorName",     "Veto");
    kwargs.setdefault("Alignable",        True);
    kwargs.setdefault("RDBAccessSvc",     "RDBAccessSvc");
    kwargs.setdefault("GeometryDBSvc",    "InDetGeometryDBSvc");
    kwargs.setdefault("GeoDbTagSvc",      "GeoDbTagSvc");
#    from AthenaCommon.DetFlags      import DetFlags
    return CfgMgr.VetoDetectorTool(name, **kwargs)


###### ComponentAccumulator

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.AthConfigFlags import AthConfigFlags
from IOVDbSvc.IOVDbSvcConfig import addFoldersSplitOnline

def ScintGeometryCfg( flags ):
    from FaserGeoModel.GeoModelConfig import GeoModelCfg
    acc,geoModelSvc = GeoModelCfg( flags )
    from GeometryDBSvc.GeometryDBSvcConf import GeometryDBSvc
    acc.addService(GeometryDBSvc("ScintGeometryDBSvc"))
    from VetoGeoModel.VetoGeoModelConf import VetoDetectorTool
    vetoDetectorTool = VetoDetectorTool()
#    vetoDetectorTool.useDynamicAlignFolders = flags.GeoModel.Align.Dynamic
    geoModelSvc.DetectorTools += [ vetoDetectorTool ]
    acc.addService(geoModelSvc)
    # if flags.GeoModel.Align.Dynamic:
    #     acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL1/ID","/Indet/AlignL1/ID",className="CondAttrListCollection"))
    #     acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL2/SCT","/Indet/AlignL2/SCT",className="CondAttrListCollection"))
    #     acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL3","/Indet/AlignL3",className="AlignableTransformContainer"))
    # else:
    #     if (not flags.Detector.SimulateSCT) or flags.Detector.OverlaySCT:
    #         acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/Align","/Indet/Align",className="AlignableTransformContainer"))
    #     else:
    #         acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/Align","/Indet/Align"))
    # if flags.Common.Project is not "AthSimulation": # Protection for AthSimulation builds
    #     if (not flags.Detector.SimulateSCT) or flags.Detector.OverlaySCT:
    #         from SCT_ConditionsAlgorithms.SCT_ConditionsAlgorithmsConf import SCT_AlignCondAlg
    #         sctAlignCondAlg = SCT_AlignCondAlg(name = "SCT_AlignCondAlg",
    #                                            UseDynamicAlignFolders = flags.GeoModel.Align.Dynamic)
    #         acc.addCondAlgo(sctAlignCondAlg)
    #         from SCT_ConditionsAlgorithms.SCT_ConditionsAlgorithmsConf import SCT_DetectorElementCondAlg
    #         sctDetectorElementCondAlg = SCT_DetectorElementCondAlg(name = "SCT_DetectorElementCondAlg")
    #         acc.addCondAlgo(sctDetectorElementCondAlg)
    return acc
