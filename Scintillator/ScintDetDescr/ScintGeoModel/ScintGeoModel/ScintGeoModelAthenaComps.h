/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#ifndef ScintGeoModel_ScintGeoModelAthenaComps_H
#define ScintGeoModel_ScintGeoModelAthenaComps_H

#include "InDetGeoModelUtils/InDetDDAthenaComps.h"

// class VetoID;


/// Class to hold various Athena components
template <class ID_HELPER>
class ScintGeoModelAthenaComps : public InDetDD::AthenaComps {

public:

  ScintGeoModelAthenaComps(const std::string& name);

  void setIdHelper(const ID_HELPER* idHelper);

  const ID_HELPER* getIdHelper() const;

private:
  const ID_HELPER* m_idHelper;

};

#include "ScintGeoModel/ScintGeoModelAthenaComps.icc"

#endif // ScintGeoModel_ScintGeoModelAthenaComps_H
