/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#ifndef SCINTGEOMODEL_VETODETECTORTOOL_H
#define SCINTGEOMODEL_VETODETECTORTOOL_H

#include "GeoModelUtilities/GeoModelTool.h"
#include "ScintGeoModel/ScintGeoModelAthenaComps.h" 

#include "GeometryDBSvc/IGeometryDBSvc.h"
#include "GeoModelInterfaces/IGeoDbTagSvc.h"
#include "RDBAccessSvc/IRDBAccessSvc.h"

#include "GaudiKernel/ServiceHandle.h"

#include <string>

namespace ScintDD {
  class VetoDetectorManager;
}

//class VetoID;
class FaserDetectorIDBase;

class VetoDetectorTool : public GeoModelTool {

public:
  // Standard Constructor
  VetoDetectorTool(const std::string& type, const std::string& name, const IInterface* parent);

  virtual StatusCode create() override final;
  virtual StatusCode clear() override final;

  // Register callback function on ConDB object
  virtual StatusCode registerCallback() override final;

  // Callback function itself
  virtual StatusCode align(IOVSVC_CALLBACK_ARGS) override;

private:
  StringProperty m_detectorName{this, "DetectorName", "Veto"};
  BooleanProperty m_alignable{this, "Alignable", true};
//   BooleanProperty m_useDynamicAlignFolders{this, "useDynamicAlignFolders", false};
  bool m_cosmic;

  const ScintDD::VetoDetectorManager* m_manager;
  
  ScintGeoModelAthenaComps<FaserDetectorIDBase> m_athenaComps;
  // ScintGeoModelAthenaComps<VetoID> m_athenaComps;

  ServiceHandle< IGeoDbTagSvc > m_geoDbTagSvc;
  ServiceHandle< IRDBAccessSvc > m_rdbAccessSvc;
  ServiceHandle< IGeometryDBSvc > m_geometryDBSvc;

//   StringProperty m_run1Folder{this, "Run1Folder", "/Indet/Align"};
//   StringProperty m_run2L1Folder{this, "Run2L1Folder", "/Indet/AlignL1/ID"};
//   StringProperty m_run2L2Folder{this, "Run2L2Folder", "/Indet/AlignL2/SCT"};
//   StringProperty m_run2L3Folder{this, "Run2L3Folder", "/Indet/AlignL3"};
};

#endif // SCINTGEOMODEL_VETODETECTORTOOL_H
